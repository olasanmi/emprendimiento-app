<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Profile;
use Faker\Generator as Faker;

$factory->define(Profile::class, function (Faker $faker) {
    return [
        'phone' => 12345,
        'direction' => 'test',
        'age' => 20,
        'profile_picture' => '123',
        'gender' => 'Male'
    ];
});
