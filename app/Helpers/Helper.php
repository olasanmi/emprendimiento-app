<?php
namespace App\Helpers;
use File;
use App\Profile;

use App\Mail\StatusChangeEmail;
use Illuminate\Support\Facades\Mail;

/**
 * 
 */
class Helper
{
	 //upload file
    public static function uploadImage($file, $pathModel) {
        $name = date('Ymd_His') . ".jpg";
        $file->move('img/'.$pathModel, $name);
        return $name;
    }

    //delete file
    public static function deleteFile($file, $pathModel) {
        $oldfile = public_path('img/'.$pathModel.'/'.$file);
        if (File::exists($oldfile)) {
            unlink($oldfile);
        }
    }
}
