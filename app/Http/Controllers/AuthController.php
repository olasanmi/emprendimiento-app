<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Carbon\Carbon;
use App\Profile;

use App\Http\Resources\User\UserResource;

class AuthController extends Controller
{
  public function signUp(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string'
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);


        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials))
            return response()->json([
                'error' => 'Unauthorized'
            ], 401);

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        
        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString(),
            'user' => new UserResource($user)
        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public function update(Request $request, $id){
        if (User::where('id', $id)->exists()) {
        $user = User::find($id);
        $user->update($request->all());

        app('App\Http\Controllers\ProfileController')->update($request, $user, $id);
        return response()->json(["message" => "Profile updated"], 202);
        }else{
        return response()->json(["message" => "Profile not found"], 404);
        }
    }

    public function destroy ($id) {
      if(User::where('id', $id)->exists()) {
        $user = User::find($id);
        $user->delete();

        return response()->json([
          "message" => "User deleted"
        ], 202);
      } else {
        return response()->json([
          "message" => "User not found"
        ], 404);
      }
    }

    public function index()
    {}

    public function show($id)
    {
    $user = User::findOrFail($id);
    return response()->json(['success' => new UserResource($user)]);
    }
    
}