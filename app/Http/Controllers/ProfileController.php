<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use App\User;
use App\Helpers\Helper;

class ProfileController extends Controller
{
    public function show(Profile $profile)
    {}

    public function edit(Profile $profile)
    {}

    public function update($request,$user, $id)
    {
        if (User::where('id', $id)->exists()) {
        $profile_id = $user->Profile->id;
        $profile = Profile::find($profile_id);
        $profile->update($request->all());
       }
}
    public function updateProfilePicture(Request $request, $id){
        if (User::where('id', $id)->exists()) {
        $user = User::find($id);
         $profile_id = $user->Profile->id;
         $profile = Profile::find($profile_id);
            if ($request->has('file')) {
                Helper::deleteFile($profile->profile_picture, 'profile_pics');
                $path = Helper::uploadImage($request->file, 'profile_pics');
                $profile->update([
                    'profile_picture' => $path]);
            }else{
        return response()->json(["message" => "File not found"], 404);
            }
        return response()->json(["message" => "Profile updated"], 202);
        }else{
        return response()->json(["message" => "Profile not found"], 404);
        }
    }
    public function destroy(Profile $profile)
    {}
}