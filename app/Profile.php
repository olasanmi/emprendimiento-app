<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //fillable
	protected $fillable = ['phone', 'direction', 'age', 'profile_picture', 'gender', 'user_id',];

    public function User () {
    	return $this->belongsTo('App\User');
    }
}
